/**
*  \file scheduler.h
*  \brief Header for scheduler.cpp
*/

#include <queue>
#include <vector>

using namespace std;

typedef unsigned int uint;

/*===================================================================
	Scheduler class
===================================================================*/

/**
 * \brief Task scheduler simulator.
 *
 * \details Simulates the scheduling of a given set of tasks using one of three
 * scheduling algorithms. When run() is called, the simulator will output a
 * table of all CPU bursts - which task was run and how long.
 * The runAll() function will create and run a simulator for each algorithm,
 * and output a comparison of the average wait and turnabout times for each
 * one.
 *
 * \author John Brink
 */
class Scheduler{
public:
	/**
	* \brief Enum of available algorithms for the Scheduler to run.
	*/
	enum SchedulerMode{
		/**
		* \brief Runs each task for up to a set time, then puts in in a queue.
		*
		* \details Requires that a time quantum be given in the Scheduler's
		* constructor.
		*/
		RoundRobin,

		/**
		* \brief Runs the highest-priority available task at all times.
		*
		* \details Requires that every task have a priority level set.
		*/
		Priority,

		/**
		* \brief Runs the shortest available task at all times.
		*
		* \details Counts shortest time remaining, not total.
		*/
		ShortestJobFirst
	};

	/*===================================================================
		Task class
	===================================================================*/

	/**
	* \brief A single task for use with the Scheduler.
	*
	* \details All member variables are public; only meant to be used by the
	* Scheduler class.
	*/
	class Task{
	public:
		/**
		* \brief The unique ID of this task, used when printing its runtime.
		*/
		int id;

		/**
		* \brief The time this task arrived.
		*/
		int arrival;

		/**
		* \brief How long this task needs to run.
		*/
		int timeTotal;

		/**
		* \brief How long this task has left to run.
		*/
		int timeRemaining;

		/**
		* \brief The priority of this task - higher number means higher priority.
		*
		* \details Ignored if Scheduler is not in priority mode.
		*/
		int priority;

		//Running data

		/**
		* \brief How long this task has waited before running (cumulative).
		*/
		int waitTime;

		/**
		* \brief The time at which this task last finished running.
		*/
		int lastRun;

		/**
		* \brief The time at which this task was completed.
		*/
		int completed;

		/**
		* \brief Creates a new task given starting information
		*
		* \param [in] num The ID of this task.
		* \param [in] arrived The time at which this task arrived.
		* \param [in] length How long this task will run.
		* \param [in] priorityLevel The priority of this task.
		*/
		Task(int num, int arrived, int length, int priorityLevel);

		/**
		* \brief Less-than operator for the Task class, used to sort tasks by
		* arrival time. Compares the arrival member.
		*/
		bool operator < (Task rhs);
	};

	/*=====================================================
		Static Methods
	=====================================================*/

	/**
	*  \brief Generates a list of tasks, then runs schedulers with each
	* of the available algrorithms and compares the results.
	*
	*  \param [in] n The number of tasks to create.
	*  \param [in] q The time quantum for the Round-Robin algorithm.
	*  \param [in] input Whether to have the user input every task's data.
	*
	*  \details If input is true, the user will have to type in every task's
	*           arrival time, length, and priority. If false, these will be
	*           randomly generated within reasonable values.
	*/
	static void testAll(int n, int q, bool input);

	/**
	*  \brief Generates a list of tasks, then runs a scheduler with the
	* selected algorithm and prints its results.
	*
	*  \param [in] mode The algorithm to test.
	*  \param [in] n The number of tasks to create.
	*  \param [in] q The time quantum for the Round-Robin algorithm.
	*  \param [in] input Whether to have the user input every task's data.
	*
	*  \details If input is true, the user will have to type in every task's
	*           arrival time, length, and priority. If false, these will be
	*           randomly generated within reasonable values.
	*/
	static void testOne(SchedulerMode mode, int n, int q, bool input);

	/*=====================================================
		Public Methods
	=====================================================*/

	/**
	*  \brief Creates a new scheduler with a set of tasks.
	*
	*  \param [in] scheduleType Which algorithm to run.
	*  \param [in] taskList The list of tasks to run on (copied, not altered).
	*
	*  \details Arbitrarily sets the time quantum to 2. For use with the
	*           Round-Robin algorithm, use the other initializer.
	*/
	Scheduler(SchedulerMode scheduleType, vector<Task> taskList);

	/**
	*  \brief Creates a new scheduler with a set of tasks and
	*  a given RR time quantum.
	*
	*  \param [in] scheduleType Which algorithm to run.
	*  \param [in] taskList The list of tasks to run on (copied, not altered).
	*  \param [in] q The time quantum for the Round-Robin algorithm. Ignored
	*              If not in Round-Robin mode.
	*/
	Scheduler(SchedulerMode scheduleType, vector<Task> taskList, int q);

	/**
	* \brief Run the scheduler with all of the added tasks
	*
	* \details Prints the duration of every task run, and then prints time,
	* throughput, average waiting time, and average turnabout at the end.
	*
	* \param [out] time How long the tasks took to finish.
	* \param [out] throughput The throughput of the scheduler.
	* \param [out] avgWaitingTime The average waiting time of the scheduler.
	* \param [out] avgTurnabout The average turnabout of the scheduler.
	*/
	void run(int &time, double &throughput, double &avgWaitingTime, double &avgTurnabout);

private:
	/*=====================================================
		Private Data
	=====================================================*/

	/**
	 * \brief The operating mode of this scheduler.
	 */
	SchedulerMode mode;

	/**
	 * \brief The time quantum used in Round-Robin mode (ignored otherwise).
	 */
	int quantum;

	/**
	 * \brief The list of tasks to schedule.
	 * Will be cleared during runtime.
	 */
	vector <Task> tasks;

	/**
	 * \brief The current time of the scheduler in arbitrary units.
	 * Counts the number of units that have passed since the scheduling began.
	 */
	int time;

	/*=====================================================
		Internal functions
	=====================================================*/

	/**
	 * \brief Prints out the header for the scheduler's output table.
	 *
	 * \details Prints the name of the scheduler's algorithm, followed by
	 * the headers of the rows printed in printRunTableEntry().
	 */
	void printRunTableHeader();

	/**
	 * \brief Prints out an entry in the scheduler's output table.
	 * If task is NULL, this runtime was spent idling.
	 *
	 * \param [in] time The current time after this CPU burst.
	 * \param [in] activeTime The length of this CPU burst.
	 * \param [in] task The task that ran during this burst,
	 *             or NULL if CPU was idle.
	 */
	void printRunTableEntry(int time, int activeTime, Task * task);

	//Run modes

	/**
	 *  \brief Runs the Round-Robin algorithm on the list of tasks.
	 *
	 *  \param [out] finished The list of tasks in their finished state.
	 *               Expects an empty vector going in.
	 *  \return The runtime of all tasks.
	 */
	int runRoundRobin(vector<Task> &finished);

	/**
	 *  \brief Runs the Priority algorithm on the list of tasks.
	 *
	 *  \param [out] finished The list of tasks in their finished state.
	 *               Expects an empty vector going in.
	 *  \return The runtime of all tasks.
	 */
	int runPriority(vector<Task> &finished);

	/**
	 *  \brief Runs the Shortest-Job-First algorithm on the list of tasks.
	 *
	 *  \param [out] finished The list of tasks in their finished state.
	 *               Expects an empty vector going in.
	 *  \return The runtime of all tasks.
	 */
	int runShortest(vector<Task> &finished);

	/**
	 *  \brief Generates a set of tasks to simulate with.
	 *
	 *  \param [in,out] tasks The list of generated tasks
	 *                  (should be empty going in).
	 *  \param [in] n The number of tasks to generate.
	 *  \param [in] input Whether to have the user input every task's values.
	 */
	static void Scheduler::generateTasks(vector<Task> &tasks, int n, bool input);

	/**
	* \brief Generates a random integer x where min <= x < max
	*
	* \param [in] min The minimum value for the result, inclusive.
	* \param [in] max The maximum value for the result, exclusive.
	*/
	static int myRand(int min, int max);
};
