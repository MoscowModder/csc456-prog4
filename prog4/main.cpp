/**
* \mainpage Program 4: OS Simulator
*
* \authors John Brink, Andrew Kizzier, Rachel Krohn
* \date 05/02/14
*
* \details **Class**: Operating Systems\n
* **Professor**: Crister Karlsson\n
* \n
*
* This program provides functions to test simulations of some of an operating
* system's management tasks. It simulates process scheduling, memory page
* replacement, and contiguous and paged memory allocation.
*
* On start, the program displays a menu prompting the user to choose one of
* the three available simulations. Once the user selects a simulation, a second
* menu is displayed prompting the user to select one of the simulator's
* algorithms or compare them all together.
*
* **Available algorithms**
* * Process Scheduling: Round-Robin, Priority, Shortest-Job-First
* * Page Replacement: First In First Out, Least Recently Used, Optimal,
*   Least-Frequently-Used, Second Chance
* * Memory Allocation: First Fit, Best Fit, Worst Fit (contiguous 
*   memory allocation); Random Victim and LRU Victim (paged memory).
*
* **Process Scheduler**
*
* The process scheduler simulates scheduling CPU time among various processes.
* The goal of the different algorithms is to strike a balance of efficiency
* (completing many tasks quickly) and fairness (finishing every task without
* too much wait time). Priority and Shortest-Job-First emphasize efficiency,
* while Round-Robin enforces fairness.
*
* **Page Replacer**
*
* The page replacement simulator illustrates the results of different page
* replacement algorithms. The goal is to minimize the page faults of the system
* to prevent unnecessary memory accessses. All the algorithms have advantages
* and disadvantages when efficiency and complexity are considered.
*
* **Contiguous and Paging Memory Allocation**
*
* The memory allocation simulator considers thedifferent strategies available
* for contiguous and paged memory allocation. For Contiguous allocation,
* first-fit is most efficient in terms of time, while worst-fit is least 
* efficient for both time and memory allocation. For paged memory allocation,
* a TLB decreases the effective access time. 
*
* **Testing**
*
* We tested the menus by entering various kinds of invalid data, like
* out-of-range numbers and letters. The program is able to identify the input
* as invalid and prompt the user repeatedly for valid input.
*
* Program results were compared to examples from the textbook and the course
* lectures to verify program correctness. Additional examples were worked by
* hand to further guarantee accuracy.
*
* We tested our simulators with the smallest and largest values possible
* (except for the scheduler which has no upper limits) to confirm that the
* program was able to handle them.
*
* **Build and Run**
*
* This program requires Visual Studio 2012 to be installed to build. Simply
* use Visual Studio's build and run button to run the program.
* Using the main menu should be self-explanatory.
*
* A pre-compiled executable is included with the package in case you do not
* have Visual Studio 2012 installed. The executable is renamed to prog4.txt;
* simply rename it to prog4.exe before running it.
*
* **Simulation and Teaching**
*
* Every simulator has an option to run and compare results from all available
* algorithms. Running the simulation many times in many different ways can help
* you to see which algorithms turn out to be the most efficient in different
* conditions. These simulators can be used to provide interactive examples of
* the various algorithms in action, which makes a better teaching tool than an
* explanation or possibly even an on-paper demonstration. Students can use the
* simulator to explore the different algorithms to encourage understanding and
* comprehension. Students could work to extend the current capabilities of the
* simulator as an assignment in an OS course.
*
* **Team Member Effort**
* * John Brink: 33.3%
* * Andrew Kizzier: 33.3% 
* * Rachel Krohn: 33.3%
*/

/**
* \todo [X] Create text menu-based version
* \todo [X] Create process scheduling simulator
* \todo [X] Create memory management simulator
* \todo [X] Create paging simulator
* \todo [_] Create GUI version (cancelled)
*
* \bug [X] Typing letters in the numeric inputs causes an infinite loop.
*/

/**
*  \file main.cpp
*  \brief Contains the main menu, which makes calls to the available
*  simulator classes.
*/

#include <iostream>
#include <string>
#include <time.h>

#include "scheduler.h"
#include "replacer.h"
#include "memory.h"

using namespace std;

/*=====================================================
	Function Prototypes
=====================================================*/
void doScheduler();
void doReplacer();
void doMemory();


/**
 * \brief Loops printing the main menu and getting user input.
 *
 * \author John Brink
 */
int main(){
	srand(time(NULL));
	
	string selection = "X";
	do{
		cout << "========================================\n"
			<< "              Main Menu                 \n"
			<< "========================================\n"
			<< "\n"
			<< "1. Process Scheduler\n"
			<< "2. Page Replacer\n"
			<< "3. Memory Management\n"
			<< "X. Quit";

		cout << "\nChoose a mode: ";
		cin >> selection;
		selection = toupper(selection[0]);
		cout << "\n\n";

		switch (selection[0]){
		case '1':
			doScheduler();
			break;

		case '2':
			doReplacer();
			break;

		case '3':
			doMemory();
			break;

		case 'X':
			cout << "Quitting.\n\n";
			break;
		default:
			cout << "Invalid input.\n\n";
			break;
		}
	} while (selection[0] != 'X');

	return 0;
}

/**
 * \brief Runs the Scheduler simulator.
 * \details Prompts the user for the number of tasks, the Round-Robin quantum,
 * and whether to manually input task details (otherwise they are randomly
 * generated).
 *
 * \author John Brink
 */
void doScheduler(){
	string selection = "";
	int taskCount = 0;
	int quantum = 0;
	bool input = false;
	string inputChar = "N";

	cout << "========================================\n"
		<< "            Scheduler Menu               \n"
		<< "========================================\n"
		<< "\n"
		<< "1. Round Robin\n"
		<< "2. Priority\n"
		<< "3. Shortest Job First\n"
		<< "4. Run all\n"
		<< "X. Quit\n";

	do{
		//Prompt for a mode
		cout << "Choose a scheduler mode: ";
		cin >> selection;
		selection = toupper(selection[0]);

	} while (selection[0] != 'X' && (selection[0] < '1' || selection[0] > '4')); //Until valid option chosen

	if (selection[0] == 'X')
	{
		cout << "Returning to main menu." << endl;
		return;
	}

	while (taskCount <= 0)
	{
		cout << "Enter the number of tasks to schedule: ";
		cin >> taskCount;
	}
	while ((selection[0] == '1' || selection[0] == '4') && quantum <= 0)
	{
		//If RR or all modes, get time quantum.
		cout << "Enter the time quantum to use: ";
		cin >> quantum;
	}
	cout << "Manually input task parameters? Y/N (default N): ";
	cin >> inputChar;
	cout << endl;
	if (toupper(inputChar[0]) == 'Y')
		input = true;

	switch (selection[0])
	{
	case '1':
		Scheduler::testOne(Scheduler::RoundRobin, taskCount, quantum, input);
		break;
	case '2':
		Scheduler::testOne(Scheduler::Priority, taskCount, 0, input);
		break;
	case '3':
		Scheduler::testOne(Scheduler::ShortestJobFirst, taskCount, 0, input);
		break;
	case '4':
		Scheduler::testAll(taskCount, quantum, input);
		break;
	}
}

/**
 * \brief Runs the Replacer simulator.
 * \details Prompts the user for which algorithm to use, then calls
 * the appropriate function in the Replacer class.
 *
 * \author Andrew Kizzier
 */
void doReplacer(){
	string selection;
	Replacer repl;
	cout << "========================================\n"
			<< "        Page Replacement Options        \n"
			<< "========================================\n"
			<< "\n"
			<< "1. First In First Out\n"
			<< "2. Least Recently Used\n"
			<< "3. Optimal\n"
			<< "4. Least Frequently Used\n"
			<< "5. Second Chance\n"
			<< "6. Comparison of all listed algorithms\n"
			<< "X. Return to Main Menu";

		cout << "\nChoose a mode: ";
		cin >> selection;
		selection = toupper(selection[0]);
		cout << "\n\n";

		switch( selection[0] )
		{
		case '1':
			repl.getInfo();
			cout << "\nRunning First In First Out algorithm\n";
			repl.fifo( false );
			break;
		case '2':
			repl.getInfo();
			cout << "\nRunning Least Recently Used algorithm\n";
			repl.lru( false );
			break;
		case '3':
			repl.getInfo();
			cout << "\nRunning Optimal algorithm\n";
			repl.optimal( false );
			break;
		case '4':
			repl.getInfo();
			cout << "\nRunning Least Frequently Used algorithm\n";
			repl.lfu( false );
			break;
		case '5':
			repl.getInfo();
			cout << "\nRunning Second Chance algorithm\n";
			repl.second_chance( false );
			break;
		case '6':
			repl.getInfo();
			cout << "\nRunning all algorithms on same data\n";
			repl.runAll();
			break;
		case 'X':
			break;
		default:
			cout << "\nInvalid input. Returning to main menu.\n";
			break;
		}
}


/**
 * \brief Runs the Contiguous memory allocation simulator.
 * \details Prompts the user for which algorithm to use, then calls
 * the appropriate function in the Contiguous class.
 *
 * \author Rachel Krohn
 */
void doMemory()
{
	string selection;
	Contiguous mem;
	Paging page;

	cout << "========================================\n"
	<< "           Memory Options               \n"
	<< "========================================\n"
	<< "\n"
	<< "1. Contiguous Process Memory\n"
	<< "2. Paged Process Memory\n"
	<< "X. Return to Main Menu\n";

	cout << "\nChoose a mode: ";
	cin >> selection;
	selection = toupper(selection[0]);
	cout << "\n\n";

	switch ( selection[0] )
	{
	case '1':
		mem.getInfo();
		mem.initData();
		mem.firstFit();
		mem.initData();
		mem.bestFit();
		mem.initData();
		mem.worstFit();
		break;
	case '2':
		page.getInfo();
		page.runRandomVictim();
		page.runLRUVictim();
		break;
	case 'X':
		break;
	default:
		cout << "Invalid input. Returning to main menu.\n";
		break;
	}
}