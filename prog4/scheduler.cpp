/**
*  \file scheduler.cpp
*  \brief Contains the classes required for process scheduling simulation.
*/

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>
#include <queue>
#include "scheduler.h"

/*===================================================================
	Task class
===================================================================*/
Scheduler::Task::Task(int num, int arrived, int length, int priorityLevel){
	id = num;
	arrival = arrived;
	timeTotal = length;
	timeRemaining = length;
	priority = priorityLevel;

	waitTime = 0;
	lastRun = arrived;
	completed = 0;
}

bool Scheduler::Task::operator < (Task rhs){
	return this->arrival < rhs.arrival;
}


/*===================================================================
	Scheduler class
===================================================================*/

/*=====================================================
	Public methods
=====================================================*/
Scheduler::Scheduler(SchedulerMode scheduleType, vector<Task> taskList){
	mode = scheduleType;
	quantum = 2;
	time = 0;
	tasks = vector<Task>(taskList);
}

Scheduler::Scheduler(SchedulerMode scheduleType, vector<Task> taskList, int q){
	mode = scheduleType;
	quantum = q;
	time = 0;
	tasks = vector<Task>(taskList);
}

void Scheduler::testAll(int n, int q, bool input){
	// Statistics
	int time;
	double stats[3][3] = { 0 };

	// Create tasks
	vector<Task> tasks;
	generateTasks(tasks, n, input);

	Scheduler prior = Scheduler(Priority, tasks);
	prior.run(time, stats[0][0], stats[0][1], stats[0][2]);

	Scheduler shortest = Scheduler(ShortestJobFirst, tasks);
	shortest.run(time, stats[1][0], stats[1][1], stats[1][2]);

	Scheduler rr = Scheduler(RoundRobin, tasks, q);
	rr.run(time, stats[2][0], stats[2][1], stats[2][2]);

	// Print statistics
	cout << "== Statistics ==" << endl
		<< setw(12) << "Runtime: " << time << endl
		<< setw(12) << "Throughput: " << stats[0][0] << endl
		<< "Algorithm   Avg Wait   Avg Turnabout" << endl;

	// For each algorithm
	for (int i = 0; i < 3; i++){
		// Print name
		switch (i)
		{
		case 0:
			cout << setw(9) << "Prior";
			break;
		case 1:
			cout << setw(9) << "SJF";
			break;
		case 2:
			cout << setw(9) << "RR";
			break;
		}
		cout << "   " << setprecision(4) << fixed
			<< setw(8) << stats[i][1] << "   "
			<< setw(13) << stats[i][2] << endl;
	}
	cout << endl << endl;
}

void Scheduler::testOne(SchedulerMode mode, int n, int q, bool input){
	// Statistics
	int time;
	double stats[3] = { 0 };

	// Create tasks
	vector<Task> tasks;
	generateTasks(tasks, n, input);

	Scheduler(mode, tasks, q).run(time, stats[0], stats[1], stats[2]);
}

void Scheduler::run(int &time, double &throughput, double &avgWaitingTime, double &avgTurnabout)
{
	if (tasks.size() == 0)
	{
		printf("No tasks to run!\n");
		return;
	}

	vector<Task> finished;

	//Print table header
	printRunTableHeader();

	//Sort by arrival time
	sort(tasks.begin(), tasks.end());

	if (mode == Priority)
	{
		time = runPriority(finished);
	}
	else if (mode == RoundRobin)
	{
		time = runRoundRobin(finished);
	}
	else if (mode == ShortestJobFirst)
	{
		time = runShortest(finished);
	}

	//Calculate statistics
	throughput = (double)finished.size() / (double)time;
	avgWaitingTime = 0;
	avgTurnabout = 0;
	for (uint i = 0; i < finished.size(); i++)
	{
		avgWaitingTime += finished[i].waitTime;
		avgTurnabout += finished[i].completed - finished[i].arrival;
	}
	avgWaitingTime /= finished.size();
	avgTurnabout /= finished.size();

	//Print statistics
	cout << endl
		<< setw(22) << "Time to finish: " << time << "\n"
		<< setw(22) << "Throughput: " << setprecision(4) << throughput << "\n"
		<< setw(22) << "Average turnabout: " << setprecision(4) << avgTurnabout << "\n"
		<< setw(22) << "Average waiting time: " << setprecision(4) << avgWaitingTime << "\n";

	cout << endl << endl;
}

/*=====================================================
	Private functions
=====================================================*/
void Scheduler::printRunTableHeader(){
	switch (mode){
	case Priority:
		cout << "== Priority ==\n";
		break;
	case RoundRobin:
		cout << "== Round Robin ==\n";
		break;
	case ShortestJobFirst:
		cout << "== Shortest Job First ==\n";
		break;
	}
	cout << "Time   Task   Duration   Remaining\n";
}

void Scheduler::printRunTableEntry(int time, int activeTime, Task * task){
	if (activeTime <= 0)
		return;

	cout << setw(4) << time << "   ";

	if (task == NULL)
		cout << setw(4) << "" << "   ";
	else
		cout << setw(4) << task->id << "   ";

	cout << setw(8) << activeTime << "   ";

	if (task == NULL)
		cout << setw(9) << "IDLE";
	else
		cout << setw(9) << task->timeRemaining;

	cout << endl;
}

/*=====================================
	Run Modes
=====================================*/
int Scheduler::runRoundRobin(vector<Task> &finished)
{
	int time = 0;
	queue<Task> que;
	uint nextToArrive = 0;


	//While tasks remain in list and queue
	while (nextToArrive < tasks.size() || que.size() > 0)
	{
		//If queue is empty and no tasks waiting, skip ahead to next-arrived task
		if (que.size() == 0 && tasks[nextToArrive].arrival > time)
		{
			int lastTime = time;
			time = tasks[nextToArrive].arrival; //Start clock at first task
			que.push(tasks[nextToArrive]);
			nextToArrive++;

			//If scheduler had to wait, print idle time
			if (time - lastTime > 0)
				printRunTableEntry(time, time - lastTime, NULL);
		}

		//Queue all newly-arrived tasks
		while (nextToArrive < tasks.size() && tasks[nextToArrive].arrival <= time)
		{
			que.push(tasks[nextToArrive]);
			nextToArrive++;
		}

		Task curTask = que.front();
		curTask.waitTime += (time - curTask.lastRun);

		//Apply the time
		int activeTime = min(quantum, curTask.timeRemaining);
		curTask.timeRemaining -= activeTime;
		time += activeTime;
		curTask.lastRun = time;

		//Print runtime info
		printRunTableEntry(time, activeTime, &curTask);

		//Is task done?
		if (curTask.timeRemaining == 0)
		{
			//If so, remove from queue and log
			curTask.completed = time;
			finished.push_back(curTask);
			que.pop();
		}
		else
		{
			//If not, move to back of queue
			que.push(curTask);
			que.pop();
		}
	}

	return time;
}

int Scheduler::runPriority(vector<Task> &finished)
{
	int time = 0;
	int nextToArrive = 0;
	int activeTask = -1;
	int curRunTime = 0; //How long active task has been running

	// While tasks remain
	while (tasks.size() > 0)
	{
		// If no active task, pick first task in list
		if (activeTask < 0){
			activeTask = 0; // Set new active task (guaranteed to be at least one)
			curRunTime = 0;

			if (tasks[activeTask].arrival > time)
			{
				// If next task hasn't arrived, idle until it does
				// Tasks sorted in arrival time order - if this hasn't arrived,
				//   then no task after it has either
				int idleTime = tasks[activeTask].arrival - time;
				time = tasks[activeTask].arrival;
				printRunTableEntry(time, idleTime, NULL);
			}
			else
			{
				// If it has arrived, see how long it waited
				tasks[activeTask].waitTime += (time - tasks[activeTask].lastRun); //Increment wait time
			}
		}

		// Check if higher-priority task arrived and switch
		for (uint i = activeTask + 1; i < tasks.size(); i++)
		{
			// Check for higher-priority task
			if (tasks[i].arrival <= time && tasks[i].priority > tasks[activeTask].priority) // Or higher-priority task
			{
				// Change task
				printRunTableEntry(time, curRunTime, &tasks[activeTask]);
				tasks[activeTask].lastRun = time;

				activeTask = i; // Set new active task
				tasks[activeTask].waitTime += (time - tasks[activeTask].lastRun); //Increment wait time
				curRunTime = 0;
			}

			// This task has not arrived - exit loop
			if (tasks[i].arrival > time)
				break;
		}

		// Run current task for 1 unit
		time++;
		curRunTime++;
		tasks[activeTask].timeRemaining--;

		// Check if task finished
		if (tasks[activeTask].timeRemaining <= 0)
		{
			printRunTableEntry(time, curRunTime, &tasks[activeTask]);
			tasks[activeTask].completed = time;
			tasks[activeTask].lastRun = time;

			// Move to finished
			finished.push_back(tasks[activeTask]); // Add to finished list
			tasks.erase(tasks.begin() + activeTask); // Remove from running list

			activeTask = -1; // No currently running task
			curRunTime = 0;
		}
	}

	return time;
}

int Scheduler::runShortest(vector<Task> &finished)
{
	int time = 0;
	int nextToArrive = 0;
	int activeTask = -1;
	int curRunTime = 0; //How long active task has been running

	// While tasks remain
	while (tasks.size() > 0)
	{
		// If no active task, pick first task in list
		if (activeTask < 0){
			activeTask = 0; // Set new active task (guaranteed to be at least one)
			curRunTime = 0;

			if (tasks[activeTask].arrival > time)
			{
				// If next task hasn't arrived, idle until it does
				// Tasks sorted in arrival time order - if this hasn't arrived,
				//   then no task after it has either
				int idleTime = tasks[activeTask].arrival - time;
				time = tasks[activeTask].arrival;
				printRunTableEntry(time, idleTime, NULL);
			}
			else
			{
				// If it has arrived, see how long it waited
				tasks[activeTask].waitTime += (time - tasks[activeTask].lastRun); //Increment wait time
			}
		}

		// Check if shorter task arrived and switch
		for (uint i = activeTask + 1; i < tasks.size(); i++)
		{
			// Check for shorter task
			if (tasks[i].arrival <= time && tasks[i].timeRemaining < tasks[activeTask].timeRemaining) // Or higher-priority task
			{
				// Change task
				printRunTableEntry(time, curRunTime, &tasks[activeTask]);
				tasks[activeTask].lastRun = time;

				activeTask = i; // Set new active task
				tasks[activeTask].waitTime += (time - tasks[activeTask].lastRun); //Increment wait time
				curRunTime = 0;
			}

			// This task has not arrived - exit loop
			if (tasks[i].arrival > time)
				break;
		}

		// Run current task for 1 unit
		time++;
		curRunTime++;
		tasks[activeTask].timeRemaining--;

		// Check if task finished
		if (tasks[activeTask].timeRemaining <= 0)
		{
			printRunTableEntry(time, curRunTime, &tasks[activeTask]);
			tasks[activeTask].completed = time;
			tasks[activeTask].lastRun = time;

			// Move to finished
			finished.push_back(tasks[activeTask]); // Add to finished list
			tasks.erase(tasks.begin() + activeTask); // Remove from running list

			activeTask = -1; // No currently running task
			curRunTime = 0;
		}
	}

	return time;
}

void Scheduler::generateTasks(vector<Task> &tasks, int n, bool input)
{
	int arrival = 0;
	int length = 0;
	int priority = 0;
	for (int i = 0; i < n; i++){
		if (input){
			//Get task values from keyboard
			cout << "Enter arrival, length, and priority for task " << i << ": ";
			cin >> arrival >> length >> priority;
			cout << endl;
		}
		else{
			//Generate random values for task
			arrival += myRand(0, 5);
			length = myRand(1, 15);
			priority = myRand(1, 5);
		}
		Task newTask(i, arrival, length, priority);

		tasks.push_back(newTask);
	}
	sort(tasks.begin(), tasks.end());

	// Print out all tasks
	cout << "Task   Arrival   Length   Priority" << endl;
	for (uint i = 0; i < tasks.size(); i++)
	{
		cout << setw(4) << tasks[i].id << "   "
			<< setw(7) << tasks[i].arrival << "   "
			<< setw(6) << tasks[i].timeTotal << "   "
			<< setw(8) << tasks[i].priority << endl;
	}
	cout << endl;
}

int Scheduler::myRand(int min, int max){
	return rand() % (max - min) + min;
}