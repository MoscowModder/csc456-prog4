/**
*  \file replacer.h
*  \brief Contains the classes required for memory replacement simulation.
*/

#include <vector>
#include <iostream>

using namespace std;

/**
 * \brief Page replacement simulator.
 * \details Simulates various algorithms used to replace pages in memory.
 * Gets information from user to run algorithm(s) selected by users.
 *
 * \author Andrew Kizzier
 */
class Replacer
{
private:
	/**
	 * \brief String containing order of memory accesses
	 */
	vector <int> refstring;

	/**
	 * \brief Array to simulate processes holding memory in pages
	 */
	int pages[20][2];

	/**
	 * \brief Number of processes for simulation
	 */
	int processes;

	/**
	 * \brief Number of pages being used
	 */
	int num_pages;

	/**
	 * \brief Number of memory accesses to simulate
	 */
	int mem_accesses;
	
	/**
	 * \brief Number of page faults
	 */
	int page_faults;

public:
	/**
	 * \brief Creates a new Replacer, with all pages set to 0.
	 */
	Replacer()
	{
		for ( int i = 0; i < 20; i++ )
			for ( int j = 0; j < 2; j++ )
				pages[i][j] = 0;
	}

	/**
	 * \brief Gets information from user for simulation use
	 * \details Also creates a random reference string
	 */
	void getInfo()
	{
		//Get number of processes to use
		cout << "Enter the number of processes to use (between 5 and 25): ";
		cin >> processes;
		//Make sure there are enough processes for use
		while( processes < 5 || processes > 25 )
		{
			cout << endl << "Enter the number of processes to use (between 5 and 25): ";
			cin >> processes;
		}
		//Get number of pages to use
		cout << endl << "Enter the number of pages to use (between 1 and 20): ";
		cin >> num_pages;
		//Make sure there are enough pages for use
		while( num_pages < 1 || num_pages > 20 )
		{
			cout << endl << "Enter the number of pages to use (between 1 and 20): ";
			cin >> num_pages;
		}

		//Get the number of memory accesses to simulate
		cout << endl << "Enter the number of memory accesses to simulate (between 20 and 50): ";
		cin >> mem_accesses;
		//Make sure there are enough accesses for use
		while( mem_accesses < 20 || mem_accesses > 50 )
		{
			cout << endl << "Enter the number of memory accesses to simulate (between 20 and 50): ";
			cin >> mem_accesses;
		}

		//Create a reference string with random numbers
		for( int i = 0; i < mem_accesses; i++ )
			refstring.push_back( rand() % processes + 1);
	}

	/**
	 * \brief Runs the First In First Out algorithm using the reference string
	 * \details Outputs each step for user to view
	 */
	void fifo( bool run_all )
	{
		bool found = false;
		int i, j;
		int oldest_time = 0;
		int oldest = 0;
		page_faults = 0;

		//Output the reference string if not running all algorithms
		if( !run_all )
		{
			cout << "\nReference string: ";
			for( i = 0; (unsigned int)i < refstring.size(); i++ )
					cout << refstring[i] << " ";
			cout << endl << endl;
		}

		//Start running simulation of fifo algortihm
		for( i = 0; (unsigned int)i < refstring.size(); i++ )
		{
			oldest_time = mem_accesses + 1;
			j = 0;
			found = false;
			//Search page table to see if process recently accessed memory
			while( !found && j < num_pages )
			{
				if( refstring[i] == pages[j][0] )
					found = true;
				j++;
			}

			//If process did not recently access memory
			if( !found )
			{
				//Search for the oldest access time
				for( j = 0; j < num_pages; j++ )
					if( oldest_time > pages[j][1] )
					{
						oldest_time = pages[j][1];
						oldest = j;
					}
				//Replace oldest access with new process
				pages[oldest][0] = refstring[i];
				pages[oldest][1] = i + 1;
				page_faults++;
			}

			//Only output if not running all algorithms
			if( !run_all )
			{
				//Output process accessing memory, page table contents, and number of page faults
				cout << "Current process accessing memory: " << refstring[i] << endl;
				cout << "Current pages: (";
				for( j = 0; j < num_pages; j++ )
				{
					cout << pages[j][0];
					if( j < num_pages - 1 )
						cout << ", ";
				}
				cout << ')' << endl;
				cout << "Current number of page faults: " << page_faults << endl << endl;
			}

		}

		cout << endl << "Total page faults for FIFO: " << page_faults << endl;
		return;
	}

	/**
	 * \brief Runs the Least Recently Used algorithm
	 * \details Outputs each step for the user to view
	 */
	void lru( bool run_all )
	{
		bool found = false;
		int i, j, loc;
		int oldest_use;
		int oldest;
		page_faults = 0;
		
		//Output the reference string if not running all algorithms
		if( !run_all )
		{
			cout << "\nReference string: ";
			for( i = 0; (unsigned int)i < refstring.size(); i++ )
					cout << refstring[i] << " ";
			cout << endl << endl;
		}

		for( i = 0; (unsigned int)i < refstring.size(); i++ )
		{
			j = 0;
			oldest_use = mem_accesses + 1;
			found = false;
			//Search page table to see if process recently accessed memory
			while( !found && j < num_pages )
			{
				if( refstring[i] == pages[j][0] )
				{
					found = true;
					loc = j;
				}
				j++;
			}

			if( !found )
			{
				//Search for the oldest access time
				for( j = 0; j < num_pages; j++ )
					if( oldest_use > pages[j][1] )
					{
						oldest_use = pages[j][1];
						oldest = j;
					}

				//Replace oldest access with new process
				pages[oldest][0] = refstring[i];
				pages[oldest][1] = i + 1;
				page_faults++;
			}
			else
			{
				pages[loc][1] = i + 1;
			}

			//Output if not running all algorithms
			if( !run_all )
			{
				//Output process accessing memory, page table contents, and number of page faults
				cout << "Current process accessing memory: " << refstring[i] << endl;
				cout << "Current pages: (";
				for( j = 0; j < num_pages; j++ )
				{
					cout << pages[j][0];
					if( j < num_pages - 1 )
						cout << ", ";
				}
				cout << ')' << endl;
				cout << "Current number of page faults: " << page_faults << endl << endl;
			}
		}

		cout << endl << "Total page faults for LRU: " << page_faults << endl;
		return;
	}


	/**
	 * \brief Runs the Least Frequently Used algorithm
	 * \details Outputs each step for the user to view
	 */
	void lfu( bool run_all )
	{
		bool found = false;
		int i, j, loc;
		int least_uses;
		int least = 0;
		page_faults = 0;
		
		//Output the reference string if not running all algorithms
		if( !run_all )
		{
			cout << "\nReference string: ";
			for( i = 0; (unsigned int)i < refstring.size(); i++ )
					cout << refstring[i] << " ";
			cout << endl << endl;
		}

		for( i = 0; (unsigned int)i < refstring.size(); i++ )
		{
			j = 0;
			least_uses = mem_accesses + 1;
			found = false;
			//Search page table to see if process recently accessed memory
			while( !found && j < num_pages )
			{
				if( refstring[i] == pages[j][0] )
				{
					found = true;
					loc = j;
				}
				j++;
			}

			if( !found )
			{
				//Search for the oldest access time
				for( j = 0; j < num_pages; j++ )
					if( least_uses > pages[j][1] )
					{
						least_uses = pages[j][1];
						least = j;
					}

				//Replace oldest access with new process
				pages[least][0] = refstring[i];
				pages[least][1] = 1;
				page_faults++;
			}
			else
			{
				pages[loc][1] = pages[loc][1] + 1;
			}

			//Only output if not running all algorithms
			if( !run_all )
			{
				//Output process accessing memory, page table contents, and number of page faults
				cout << "Current process accessing memory: " << refstring[i] << endl;
				cout << "Current pages: (";
				for( j = 0; j < num_pages; j++ )
				{
					cout << pages[j][0];
					if( j < num_pages - 1 )
						cout << ", ";
				}
				cout << ')' << endl;
				cout << "Current number of page faults: " << page_faults << endl << endl;
			}
		}

		cout << endl << "Total page faults for LFU: " << page_faults << endl;
		return;
	}

	/**
	 * \brief Runs the Optimal algorithm
	 * \details Outputs each step for the user to view
	 */
	void optimal( bool run_all )
	{
		bool found = false;
		int i, j, k;
		int farthest;
		int farthest_dist;
		page_faults = 0;

		//Output the reference string if not running all algorithms
		if( !run_all )
		{
			cout << "\nReference string: ";
			for( i = 0; (unsigned int)i < refstring.size(); i++ )
					cout << refstring[i] << " ";
			cout << endl << endl;
		}

		for( i = 0; (unsigned int)i < refstring.size(); i++ )
		{
			j = 0;
			found = false;
			//Search page table to see if process recently accessed memory
			while( !found && j < num_pages )
			{
				if( refstring[i] == pages[j][0] )
					found = true;
				j++;
			}

			if( !found )
			{
				farthest_dist = -1;
				for( j = 0; j < num_pages; j++ )
				{
					k = i;
					//look for next use by process
					while( (unsigned int)k < refstring.size() )
					{
						if( pages[j][0] == refstring[k] )
						{
							pages[j][1] = k;
							k = refstring.size() + 1;
						}
						//if process no longer in reference string
						if( k + 1 == refstring.size() && pages[j][0] != refstring[k] )
							pages[j][1] = refstring.size() + 1;
						k++;
					}	
					
					if( pages[j][1] > farthest_dist )
					{
						farthest = j;
						farthest_dist = pages[j][1];
					}
					//if page table is empty
					if( pages[j][0] == 0 )
					{
						farthest = j;
						j = num_pages + 1;
					}

				}

				pages[farthest][0] = refstring[i];
				page_faults++;
			}

			//Only output if not running all algorithms
			if( !run_all )
			{
				//Output process accessing memory, page table contents, and number of page faults
				cout << "Current process accessing memory: " << refstring[i] << endl;
				cout << "Current pages: (";
				for( j = 0; j < num_pages; j++ )
				{
					cout << pages[j][0];
					if( j < num_pages - 1 )
						cout << ", ";
				}
				cout << ')' << endl;
				cout << "Current number of page faults: " << page_faults << endl << endl;
			}
		}

		cout << endl << "Total page faults for OPT: " << page_faults << endl;
		return;
	}

	/**
	 * \brief Runs the Second Chance algorithm
	 * \details Outputs each step for the user to view
	 */
	void second_chance( bool run_all )
	{
		bool found = false;
		int i, j;
		int current_loc = 0;
		page_faults = 0;

		//Output the reference string if not running all of them
		if( !run_all )
		{
			cout << "\nReference string: ";
			for( i = 0; (unsigned int)i < refstring.size(); i++ )
					cout << refstring[i] << " ";
			cout << endl << endl;
		}

		//Start running simulation of second chance algortihm
		for( i = 0; (unsigned int)i < refstring.size(); i++ )
		{
			j = 0;
			found = false;
			//Search page table to see if process recently accessed memory
			while( !found && j < num_pages )
			{
				if( refstring[i] == pages[j][0] )
				{
					found = true;
					pages[j][1] = 1;
				}
				j++;
			}

			//If process did not recently access memory
			if( !found )
			{
				if( current_loc == num_pages )
					current_loc = 0;
				for( j = current_loc; j < num_pages; j++ )
				{
					if( pages[j][1] == 1 )
						pages[j][1] = 0;
					else if( pages[j][1] == 0 )
					{
						pages[j][0] = refstring[i];
						pages[j][1] = 1;
						current_loc = j + 1;
						j = num_pages + 1;
						page_faults++;
					}

					if( j == num_pages - 1 && pages[j][1] == 0 )
						j = -1;
				}
			}

			//Only output if not running all algorithms
			if( !run_all )
			{
				//Output process accessing memory, page table contents, and number of page faults
				cout << "Current process accessing memory: " << refstring[i] << endl;
				cout << "Current pages: (";
				for( j = 0; j < num_pages; j++ )
				{
					cout << pages[j][0];
					if( j < num_pages - 1 )
						cout << ", ";
				}
				cout << ')' << endl;
				cout << "Current number of page faults: " << page_faults << endl << endl;

				//Output the reference bits
				cout << "Current reference bits: ";
				for( j = 0; j < num_pages; j++ )
					cout << pages[j][1] << " ";
				cout << endl << endl;
			}
		}

		cout << endl << "Total page faults for Second Chance: " << page_faults << endl;
		return;
	}

	/**
	 * \brief Runs all of the available algorithms in order.
	 * \details Runs fifo(), lru(), optimal(), lfu(), and second_chance().
	 */
	void runAll()
	{
		//Output the reference string
		cout << "\nReference string: ";
		for( int i = 0; (unsigned int)i < refstring.size(); i++ )
				cout << refstring[i] << " ";
		cout << endl << endl;
		//Run fifo algorithm
		fifo( true );
		//Run algorithm
		lru( true );
		//Run optimal algorithm
		optimal( true );
		//Run lfu algorithm
		lfu( true );
		//Run second chance algorithm
		second_chance( true );
	}
};
