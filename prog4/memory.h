/**
*  \file memory.h
*  \brief Contains the classes required for memory allocation simulation. 
*/

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * \brief Simulator for placing contiguous sections of data in memory. The 
 *  Contiguous class simulates contiguous memory allocation using the first-fit,
 *  best-fit, and worst-fit algorithms. The user enters the number of processes
 *  and the number of partitions. The program then generates random process and
 *  partion sizes before running the simulations. For each placement strategy,
 *  the simulation outputs the available space in the partions at each step 
 *  after displaying the size of the processes.
 * 
 * \author Rachel Krohn
 */
class Contiguous 
{
private:

	/**
	 * \brief Struct for each memory partition
	 */
	struct partition
	{
		int available;
		int used;
	};


	/**
	 * \brief Number of processes for simulation
	 */
	int num_processes;

	/**
	 * \brief Number of partitions being used
	 */
	int num_partitions;

	/**
	 * \brief List of process memory sizes
	 */
	int process_sizes[15];

	/**
	 * \brief Boolean array marking process as allocated
	 */
	bool process_used[15];

	/**
	 * \brief Array of memory partition sizes
	 */
	int partition_sizes[20];

	/**
	 * \brief Array of memory partitions
	 */
	partition memory[20];

	

public:

	/**
	 * \brief Empty initializer.
	 */
	Contiguous(){}

	/**
	 * \brief Initializes the object's instance variables.
	 */
	void initData()
	{
		//set all processes as unallocated
		for ( int i = 0; i < num_processes; i++ )
			process_used[i] = false;

		//set used and available for all memory partitions
		for ( int i = 0; i < num_partitions; i++ )
		{
			memory[i].available = partition_sizes[i];
			memory[i].used = 0;
		}
	}

	/**
	 * \brief Prompts the user to input parameters for the simulation.
	 */
	void getInfo()
	{
		//Get number of processes to use
		cout << "Enter the number of processes to use (between 5 and 15): ";
		cin >> num_processes;
		//Make sure there are enough processes for use
		while( num_processes < 5 || num_processes > 15 )
		{
			cout << endl << "Enter the number of processes to use (between 5 and 15): ";
			cin >> num_processes;
		}
		//Get number of memory partitions
		cout << endl << "Enter the number of memory partitions (between 5 and 15): ";
		cin >> num_partitions;
		//Make sure there are enough pages for use
		while( num_partitions < 1 || num_partitions > 15 )
		{
			cout << endl << "Enter the number of pages to use (between 5 and 15): ";
			cin >> num_partitions;
		}

		//Generate partition sizes: 100 to 1000 in increments of 10
		for ( int i = 0; i < num_partitions; i++ )
			partition_sizes[i] = 10 * ( rand() % 91 + 10 );

		//Generate process sizes: 100 to 500
		for ( int i = 0; i < num_processes; i++ )
			process_sizes[i] = rand() % 401 + 100;
	}

	/**
	 * \brief Runs the First-Fit algorithm for contiguous memory allocation.
	 */
	void firstFit()
	{
		int j;		//partition iterator

		cout << endl << endl << "*** FIRST FIT ALGORITHM ***" << endl << endl;
		cout << "Process sizes: ";
		for ( int i = 0; i < num_processes; i++ )
			cout << process_sizes[i] << ", ";
		cout << endl << endl;
		cout << "Available partition space at each step:" << endl;
		cout << "                   | ";
		for ( int i = 0; i < num_partitions; i++ )
			cout << setw(4) << partition_sizes[i] << " | ";
		cout << endl << endl;

		for ( int i = 0; i < num_processes; i++ )	//loop process
		{
			j = 0;		//reset partition index
			//loop partition to find hole
			while ( memory[j].available < process_sizes[i] && j < num_partitions )	
				j++;

			//found hole, allocate process and print results
			if ( j < num_partitions )
			{
				memory[j].used += process_sizes[i];		//mark space used
				memory[j].available -= process_sizes[i];	//decrease available space
				process_used[i] = true;		//mark process as allocated

				//print allocation results
				cout << "allocating " << process_sizes[i] << " KB  | ";
				for ( int k = 0; k < num_partitions; k++ )
					cout << setw(4) << memory[k].available << " | ";
				cout << endl;
			}
		}
		cout << endl << endl;

		//print any unallocated processes
		for ( int i = 0; i < num_processes; i++ )
		{
			if ( !process_used[i] )
				cout << "Could not allocate process of size " << process_sizes[i] << " KB" << endl << endl;
		}
	}

	/**
	 * \brief Runs the Best-Fit algorithm for contiguous memory allocation.
	 */
	void bestFit()
	{
		int smallest;		//index of smallest partition that will contain process

		cout << endl << endl << "*** BEST FIT ALGORITHM ***" << endl << endl;
		cout << "Process sizes: ";
		for ( int i = 0; i < num_processes; i++ )
			cout << process_sizes[i] << ", ";
		cout << endl << endl;
		cout << "Available partition space at each step:" << endl;
		cout << "                   | ";
		for ( int i = 0; i < num_partitions; i++ )
			cout << setw(4) << partition_sizes[i] << " | ";
		cout << endl << endl;

		for ( int i = 0; i < num_processes; i++ )	//loop process
		{
			smallest = -1;	//reset index of smallest partition

			//loop partition to find smallest hole large enough for process
			for ( int j = 0; j < num_partitions; j++ )
			{
				//process fits, check hole against previous
				if ( memory[j].available > process_sizes[i] ) 
				{
					//first hole found, or new hole is smaller: save
					 if ( smallest == -1 || memory[j].available < memory[smallest].available )
						smallest = j;
				}
			}

			//found hole, allocate process and print results
			if ( smallest != -1 )
			{
				memory[smallest].used += process_sizes[i];		//mark space used
				memory[smallest].available -= process_sizes[i];	//decrease available space
				process_used[i] = true;		//mark process as allocated

				//print allocation results
				cout << "allocating " << process_sizes[i] << " KB  | ";
				for ( int k = 0; k < num_partitions; k++ )
					cout << setw(4) << memory[k].available << " | ";
				cout << endl;
			}
		}
		cout << endl << endl;

		//print any unallocated processes
		for ( int i = 0; i < num_processes; i++ )
		{
			if ( !process_used[i] )
				cout << "Could not allocate process of size " << process_sizes[i] << " KB" << endl << endl;
		}
	}


	/**
	 * \brief Runs the Worst-Fit algorithm for contiguous memory allocation.
	 */
	void worstFit()
	{
		int largest;		//index of largest partition that will contain process

		cout << endl << endl << "*** WORST FIT ALGORITHM ***" << endl << endl;
		cout << "Process sizes: ";
		for ( int i = 0; i < num_processes; i++ )
			cout << process_sizes[i] << ", ";
		cout << endl << endl;
		cout << "Available partition space at each step:" << endl;
		cout << "                   | ";
		for ( int i = 0; i < num_partitions; i++ )
			cout << setw(4) << partition_sizes[i] << " | ";
		cout << endl << endl;

		for ( int i = 0; i < num_processes; i++ )	//loop process
		{
			largest = -1;	//reset index of smallest partition

			//loop partition to find smallest hole large enough for process
			for ( int j = 0; j < num_partitions; j++ )
			{
				//process fits, check hole against previous
				if ( memory[j].available > process_sizes[i] ) 
				{
					//first hole found, or new hole is smaller: save
					 if ( largest == -1 || memory[j].available > memory[largest].available )
						largest = j;
				}
			}

			//found hole, allocate process and print results
			if ( largest != -1 )
			{
				memory[largest].used += process_sizes[i];		//mark space used
				memory[largest].available -= process_sizes[i];	//decrease available space
				process_used[i] = true;		//mark process as allocated

				//print allocation results
				cout << "allocating " << process_sizes[i] << " KB  | ";
				for ( int k = 0; k < num_partitions; k++ )
					cout << setw(4) << memory[k].available << " | ";
				cout << endl;
			}
		}
		cout << endl << endl;

		//print any unallocated processes
		for ( int i = 0; i < num_processes; i++ )
		{
			if ( !process_used[i] )
				cout << "Could not allocate process of size " << process_sizes[i] << " KB" << endl << endl;
		}
	}

};

/**
 * \brief Simulator for paging and TLB of program memory. The Paging class 
 * prompts the user for the number of pages in memory and in the page table,
 * the number of entries in the TLB, the number of page accesses for the
 * simulation, and the time for a single memory access. The number of TLB 
 * entries is must fall between 1/4 and 1/2 of the page table entries, to
 * better approximate a small cache-based TLB implementation. The 
 * simulation generates the specified number of page accesses. These
 * values are generated such that some pages are accessed more frequently 
 * than others, since in a real OS page access frequencies vary. The 
 * simulation then "runs" the page accesses, checking to see if the page
 * is in the TLB or not. Based on the simulation results, the effective
 * access time with or without the TLB is calculated, along with the hit
 * rate of the TLB. The results of individual page accesses are not 
 * displayed because of space.
 *
 * \author Rachel Krohn
 */
class Paging 
{
private:
	/**
	 * \brief Number of memory pages for simulation.
	 */
	int num_pages;

	/**
	 * \brief Number of TLB entries for simulation.
	 */
	int num_TLB;

	/**
	 * \brief Number of page accesses for simulation.
	 */
	int num_accesses;

	/**
	 * \brief Page accesses for simulation.
	 */
	int access[1000];

	/**
	 * \brief Page access frequency guides, so some pages are accessed more than others.
	 */
	int frequencies[50];

	/**
	 * \brief Time for each memory access.
	 */
	int access_time;

	/**
	 * \brief TLB for random victim simulation.
	 */
	vector <int> TLB_random;

	/**
	 * \brief TLB for LRU victim simulation.
	 */
	vector <int> TLB_LRU;

	/**
	 * \brief Number of TLB hits.
	 */
	int TLB_hits;

public:
	/**
	 * \brief Prompts the user to input parameters for the simulation.
	 */
	void getInfo()
	{
		int new_access;			//new page for initializing TLB
		float eat_page;			//effective access time without TLB

		//Get number of pages to use
		cout << "Enter the number of pages to use (between 20 and 50): ";
		cin >> num_pages;
		//Make sure there are enough pages for use
		while( num_pages < 20 || num_pages > 50 )
		{
			cout << "Ivalid input. Enter the number of pages to use (between 20 and 50): ";
			cin >> num_pages;
		}

		//Get number of TLB entries
		cout << endl << "Enter the number of TLB entries (between " << num_pages/4 << " and " << num_pages / 2 << "): ";
		cin >> num_TLB;
		//Make sure there are enough TLB entries for use
		while( num_TLB < num_pages/4 || num_TLB > num_pages / 2 )
		{
			cout << "Invalid input. Enter the number of TLB entries (between " << num_pages/4 << " and " << num_pages / 2 << "): ";
			cin >> num_TLB;
		}

		//Get number of page accesses for simulation
		cout << endl << "Enter the number of page accesses (between 50 and 1000): ";
		cin >> num_accesses;
		//Make sure there are enough page accesses for use
		while( num_accesses < 50 || num_accesses > 1000 )
		{
			cout << "Invalid input. Enter the number of page accesses (between 50 and 1000): ";
			cin >> num_accesses;
		}

		//Get time for memory access
		cout << endl << "Enter the time for memory access in nanoseconds (between 50 and 300): ";
		cin >> access_time;
		//Make sure access time is appropriate
		while( access_time < 50 || access_time > 300 )
		{
			cout << "Invalid intput. Enter the time for memory access in nanoseconds (between 50 and 300): ";
			cin >> access_time;
		}

		//Generate frequency guides for each page, so that some pages are accessed more than others
		//look for high variation since some pages are accessed only 2 or 3 times, while others are accessed very frequently
		for ( int i = 0; i < num_pages; i++ )
		{
			frequencies[i] = ( rand() % 100 ) + 1;	//get random numbers 1 to 100
			frequencies[i] *= 0.5 * frequencies[i];		//square and scale result to get greater variation
			if ( i != 0 )
				frequencies[i] += frequencies[i-1];		//frequencies are cumulative
		}

		//Generate page accesses based on page weights
		for ( int i = 0; i < num_accesses; i++ )
		{
			int random = rand() % frequencies[num_pages-1];		//random number less than maximum cumulative weight
			for ( int j = 0; j < num_pages; j++ )		//loop weights
			{
				//if random number less than current cumulative weight, use corresponding page for access
				if ( random <= frequencies[j] )
				{
					access[i] = j + 1;
					break;
				}
			}
		}

		//Generate random data to initialize TLB
		for ( int i = 0; i < num_TLB; i++ )
		{
			//generate new pages until get page not already in TLB
			new_access = ( rand() % num_pages ) + 1;	
			while ( i != 0 && find( TLB_LRU.begin(), TLB_LRU.end(), new_access ) != TLB_LRU.end() )
				new_access = ( rand() % num_pages ) + 1;
			//store new page in both TLBs
			TLB_LRU.push_back( new_access );
			TLB_random.push_back( new_access );
		}

		eat_page = 2 * access_time;
		cout << endl << endl << "Effective Access Time without TLB: " << eat_page << endl;

	}

	/**
	 * \brief Runs TLB paging simulation with victims randomly selected.
	 */
	void runRandomVictim()
	{
		float hit_ratio;		//hit ratio based on simulation results
		float eat_TLB;			//effective access time based on simulation results with TLB

		TLB_hits = 0;		//set number of hits to 0

		for ( int i = 0; i < num_accesses; i++ )
		{
			vector<int>::iterator it;			//iterator for TLB vector
			it = find( TLB_random.begin(), TLB_random.end(), access[i] );		//search TLB for page of interest

			//found page in TLB
			if ( it != TLB_random.end() )
				TLB_hits++;			//add one to hit counter

			//page not in TLB, add page to TLB
			else
			{
				int victim = rand() % num_TLB;		//randomly select a TLB entry to replace
				TLB_random[victim] = access[i];
			}
		}

		//calculate simulation statistics
		hit_ratio = (float) TLB_hits / (float) num_accesses;
		eat_TLB = hit_ratio * (float) access_time + ( 1 - hit_ratio ) * 2 * (float) access_time;

		//print simulation results
		cout << endl << endl << "Simulation Results: Random TLB Victim" << endl;
		cout <<	"=====================================" << endl;
		cout << "Number of TLB hits: " << TLB_hits << endl;
		cout << "Hit Ratio: " << hit_ratio << endl;
		cout << "Effective Access Time with TLB: " << eat_TLB << endl << endl << endl;
	}

	/**
	 * \brief Prompts the user to input parameters for the simulation.
	 */
	void runLRUVictim()
	{
		float hit_ratio;		//hit ratio based on simulation results
		float eat_TLB;			//effective access time based on simulation results with TLB

		TLB_hits = 0;		//set number of hits to 0

		for ( int i = 0; i < num_accesses; i++ )
		{
			vector<int>::iterator it;			//iterator for TLB vector
			it = find( TLB_LRU.begin(), TLB_LRU.end(), access[i] );		//search TLB for page of interest

			//found page in TLB
			if ( it != TLB_LRU.end() )
			{
				TLB_hits++;						//add one to hit counter
				TLB_LRU.erase( it );			//remove page from TLB
				TLB_LRU.push_back( access[i] );	//replace at end of queue
			}

			//page not in TLB, add page to TLB
			else
			{
				TLB_LRU.erase( TLB_LRU.begin() );	//remove first element
				TLB_LRU.push_back( access[i] );		//put current access at end of queue
			}
		}

		//calculate simulation statistics
		hit_ratio = (float) TLB_hits / (float) num_accesses;
		eat_TLB = hit_ratio * (float) access_time + ( 1 - hit_ratio ) * 2 * (float) access_time;

		//print simulation results
		cout << endl << "Simulation Results: LRU TLB Victim" << endl;
		cout <<	"=====================================" << endl;
		cout << "Number of TLB hits: " << TLB_hits << endl;
		cout << "Hit Ratio: " << hit_ratio << endl;
		cout << "Effective Access Time with TLB: " << eat_TLB << endl << endl << endl;
	}
};